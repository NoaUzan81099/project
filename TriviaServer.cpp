#include "TriviaServer.h" //bye bye
#include <vector>
#include "Validator.h"
#include <fstream>

// makes the db
// creates new socket (if not - throws exception)
TriviaServer::TriviaServer()
{
	_roomIdSequence = 0;

	// create new socket
	_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
		throw exception(__FUNCTION__ " - socket");
}

// deletes the rooms list
// deletes the connected users
// closes all opened socket
TriviaServer::~TriviaServer()
{
	// delete the rooms list
	std::map<int, Room*>::iterator itRooms = _roomsList.begin();
	for (; itRooms != _roomsList.end(); ++itRooms)
		delete itRooms->second;

	// delete the conected users list
	std::map<SOCKET, User*>::iterator itUsers = _connectedUsers.begin();
	for (; itUsers != _connectedUsers.end(); ++itUsers)
	{
		closesocket(itUsers->first); // close all users sockets
		delete itUsers->second;
	}

	// close the main socket
	TRACE(__FUNCTION__ " closing accepting socket");
	try
	{
		::closesocket(_socket);
	}
	catch (...) {}
}

void TriviaServer::serve()
{
	bindAndListen();

	// create the thread that handles the recieved messages queue
	thread handleRequestsQueue(&TriviaServer::handleRecievedMessages, this);
	handleRequestsQueue.detach();
	
	while (true)
	{
		// the main thread is only accepting clients 
		// and add them to the list of handlers
		TRACE("accepting client...");
		accept();
	}
}

////////////////////////////////
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

// listen to connecting requests from clients
// accept them, and create thread for each client
void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(Protocol::PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = Protocol::IFACE;

	if (::bind(_socket, (struct sockaddr*)&sa, sizeof sa) == SOCKET_ERROR)
		throw exception(__FUNCTION__ " - bind");

	TRACE("binded");

	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw exception(__FUNCTION__ " - listen");

	TRACE("listening...");
}

void TriviaServer::accept()
{
	SOCKET client_socket = ::accept(_socket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	TRACE("Client accepted !");

	// create new thread for client	and detach from it
	thread tr(&TriviaServer::clientHandler, this, client_socket);
	tr.detach();
}

void TriviaServer::clientHandler(SOCKET sock)
{
	int typeCode = Helper::getMessageTypeCode(sock);

	try
	{
		while (typeCode != Protocol::RQST_EXIT && typeCode != 0)
		{
			addRecievedMessage(buildRecieveMessage(sock, typeCode));
			typeCode = Helper::getMessageTypeCode(sock);
		}
	}
	catch (...) 
	{
		TRACE(__FUNCTION__ ": Problem with client ");
	}

	addRecievedMessage(buildRecieveMessage(sock, Protocol::RQST_EXIT));
}

void TriviaServer::safeDeleteUser(RecievedMessage* msg)
{
	throw NotImplementedException();
}

////////////////////////////////
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

//need to add success and error messages

User* TriviaServer::handleSignin(RecievedMessage* msg)
{
	vector<string> v = msg->getValues();
	string line;
	string substrings[2];
	size_t pos;
	fstream f;
	bool isOk=false;
	int len;
	f.open("user.text");
	while (!f.eof())
	{
		getline(f, line);
		pos = line.find(",");
		substrings[1]=line.substr(pos);
		len = line.length();
		len -= substrings[1].length();
		substrings[0] = line.substr(0, len);
		if (substrings[0] == v[0])
		{
			if (substrings[1] == v[1])
			{
				isOk = true;
			}
		}
	}
	if (isOk == false)
	{
		//error message
	}
	else
	{
		//success!!!!!!!!
	}
	f.close();

	/// DEBUG:
	return nullptr;
}

bool TriviaServer::handleSignup(RecievedMessage* msg)
{
	vector<string> v = msg->getValues();
	string line;
	string substrings[2];
	size_t pos;
	fstream f;
	bool unExsits = false, passExisits = false;
	int len;
	f.open("user.text");
	bool pass = Validator::isPasswordValid(v[1]);
	if (pass == false)
	{
		//send error message
	}
	bool u = Validator::isUsernameValid(v[0]);
	if (u == false)
	{
		//send error message
	}
	if (u == true && pass == true)
	{
		while (!f.eof())
		{
			getline(f, line);
			pos = line.find(",");
			substrings[1] = line.substr(pos);
			len = line.length();
			len -= substrings[1].length();
			substrings[0] = line.substr(0, len);
			if (substrings[0] == v[0])
			{
				unExsits = true;
			}
		}
		if (unExsits == true)
		{
			//error message
		}
		else
		{
			f << v[1] + "," + v[2] + "\n";
			//success!!!!!!!!
		}
	}
	f.close();
	
	//need to check if exsits by DB
	//if not need to add it to DB
	//if error send error message
	//if succees send success message

	/// DEBUG:
	return true;
}

void TriviaServer::handleSignout(RecievedMessage* msg)
{
	User* u = msg->getUser();
	SOCKET s;

	if (u != nullptr)
	{
		handleLeaveRoom(msg);
		handleCloseRoom(msg);
		handleLeaveGame(msg);
		s = u->getSocket();
		map<SOCKET, User*>::iterator it;
		for (it = _connectedUsers.begin(); it != _connectedUsers.end(); it++)
		{
			if (it->first == s)
			{
				break;
			}
		}
		_connectedUsers.erase(it);
	}
}

//need to realse game memory
////////////////////////////////
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\//
void TriviaServer::handleLeaveGame(RecievedMessage* msg)
{
	bool b;
	User* u=msg->getUser();
	b=u->leaveGame();
	if(b==true)
	{
		//need to release memory
	}
}

void TriviaServer::handleStartGame(RecievedMessage* msg)
{
	throw NotImplementedException();
}

void TriviaServer::handlePlayerAnswer(RecievedMessage* msg)
{
	throw NotImplementedException();
}

////////////////////////////////
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

// handle the "create room" request (code: 213)
// @returns TRUE if succeded, FALSE otherwise
bool TriviaServer::handleCreateRoom(RecievedMessage* msg)
{
	// get the related user for the socket
	msg->setUser(getUserBySocket(msg->getSock()));

	User* userPtr = msg->getUser(); // just to call getUser() only once

	if (userPtr) // if: user exsists
	{
		if (!(msg->getValues().empty())) // if: msg has values
		{
			_roomIdSequence++;

			// call "create room" on the user
			int roomNameSize = 0;
			string roomName("ariel test");
			int playersNum = 0;
			int questionsNum = 0;
			int questionTimeInSec = 0;

			bool retVal = userPtr->createRoom(roomNameSize, roomName,
				playersNum, questionsNum, questionTimeInSec);

			if (retVal) // function "createRoom" succeded
			{
				// add the room to the rooms list
				userPtr->getRoom();
				_roomsList.insert (
					pair<int, Room*> (
						userPtr->getRoom()->getId(), userPtr->getRoom()
					));
			}
		}
	}

	return false;
}

// handle the "close room" request (code: 215)
// @returns TRUE if succeded, FALSE otherwise
bool TriviaServer::handleCloseRoom(RecievedMessage* msg)
{
	// get the related user for the socket
	msg->setUser(getUserBySocket(msg->getSock()));

	User* userPtr = msg->getUser(); // just to call getUser() only once

	if (userPtr) // if: user exsists
	{
		// get users room
		Room* roomPtr = userPtr->getRoom();

		if (roomPtr) // if: room exsists
		{
			// call "close room" on the user			
			if (userPtr->closeRoom() != -1)
			{
				// delete the room from the rooms list
				_roomsList.erase(userPtr->getRoom()->getId());

				return true;
			}
		}
	}

	return false;

}

// handle the "join room" request (code: 209)
// @returns TRUE if succeded, FALSE otherwise
// msg: [209roomID]
bool TriviaServer::handleJoinRoom(RecievedMessage* msg)
{
	// get the related user for the socket
	msg->setUser(getUserBySocket(msg->getSock()));

	User* userPtr = msg->getUser(); // just to call getUser() only once

	if (userPtr) // if: user exsists
	{
		int roomId = Helper::getIntPartFromSocket(msg->getSock(), 4);
		Room* roomPtr = getRoomById(roomId);

		if (roomPtr) // if: room exsists
		{
			// join the user to the room
			return userPtr->joinRoom(roomPtr);
		}
		else // failed - room not exist or other reason [1102]
		{
			// send failure message to the user
			Helper::sendData(userPtr->getSocket(), "1102");
		}
	}

	return false;
}

// handle the "leave room" request (code: 211)
// @returns TRUE if succeded, FALSE otherwise
bool TriviaServer::handleLeaveRoom(RecievedMessage* msg)
{
	// get the related user for the socket
	msg->setUser(getUserBySocket(msg->getSock()));
	
	User* userPtr = msg->getUser(); // just to call getUser() only once

	if (userPtr) // if: user exsists
	{
		// get the related room for the user
		if (userPtr->getRoom()) // if: user in a room
		{
			// activate User.leaveRoom()
			userPtr->leaveRoom();
			return true;
		}
	}

	return false;

}

void TriviaServer::handleGetUsersInRoom(RecievedMessage* msg)
{
	User* u = msg->getUser();
	vector<string> v = msg->getValues();
	string mes;
	Room* r = getRoomById(stoi(v[0]));
	if (r == nullptr)
	{
		//send error message
	}
	else
	{
		mes = r->getUsersListMessage();
		//need to send mes to client
	}

}

void TriviaServer::handleGetRooms(RecievedMessage* msg)
{
	User* u = msg->getUser();
	vector<string> v = msg->getValues();
	int overAllLen = _roomsList.size();
	int RoomId;
	string nameLen,roomName;
	map<int, Room*>::iterator it = _roomsList.begin();
	string finalMess="106"+to_string(overAllLen);

	for (; it != _roomsList.end(); it++)
	{
		RoomId = it->second->getId();
		roomName = it->second->getName();
		if (nameLen.length() < 10)
		{
			nameLen = "0" + to_string(nameLen.length());
		}
		else
		{
			nameLen = to_string(nameLen.length());
		}
		finalMess += nameLen + to_string(RoomId);		
	}
	//need to send finalMess
		
			
}

////////////////////////////////
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

void TriviaServer::handleGetBestScores(RecievedMessage* msg)
{
	throw NotImplementedException();
}

void TriviaServer::handleGetPersonalStatus(RecievedMessage* msg)
{
	throw NotImplementedException();
}

////////////////////////////////
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

void TriviaServer::handleRecievedMessages()
{
	throw NotImplementedException();
	ulock = unique_lock<mutex>(_mtxRecievedMessages, defer_lock);
	
	// if not empty
	if (!_queRcvMessages.empty())
	{

	}

}

void TriviaServer::addRecievedMessage(RecievedMessage* msg)
{
	unique_lock<mutex> ulock(_mtxRecievedMessages);
	_queRcvMessages.push_back(msg);
	ulock.unlock();

	_handleRecievedMessage.notify_one();
}

RecievedMessage* TriviaServer::buildRecieveMessage(SOCKET, int)
{
	throw NotImplementedException();
}

////////////////////////////////
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

// seeks for the user in the connected users list by his NAME
// @returns User* OR nullptr
User* TriviaServer::getUserByName(string name)
{
	map<SOCKET, User*>::iterator it = _connectedUsers.begin();

	for (; it != _connectedUsers.end(); it++)
		if (it->second->getUsername() == name)
			return it->second; // if found -> return the user ptr

	return nullptr; // if not found
}

// seeks for the user in the connected users list by his SOCKET
// @returns User* OR nullptr
User* TriviaServer::getUserBySocket(SOCKET sock)
{
	map<SOCKET, User*>::iterator it = _connectedUsers.find(sock);

	if (it != _connectedUsers.end()) // user found
		return it->second; // return the user ptr

	return nullptr; // if not found
}

// seeks for the room by his id
// @returns Room* OR nullptr
Room* TriviaServer::getRoomById(int id)
{
	map<int, Room*>::iterator it = _roomsList.find(id);

	if (it != _roomsList.end()) // room found
		return it->second; // return the room ptr

	return nullptr; // if not found
}

