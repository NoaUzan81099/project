#include "User.h"


User::User(string username, SOCKET sock)
{
	_username = username;
	_sock = sock;

	_currRoom = nullptr;
	_currGame = nullptr;
}

void User::send(string msg)
{
	Helper::sendData(_sock, msg);
}

string User::getUsername()
{
	return _username;
}

SOCKET User::getSocket()
{
	return _sock;
}

Room* User::getRoom()
{
	return _currRoom;
}

Game* User::getGame()
{
	return _currGame;
}

void User::setGame(Game* newGame)
{
	_currRoom = nullptr;
	_currGame = newGame;
}

void User::clearRoom()
{
	_currRoom = nullptr;
}

bool User::createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime)
{
	throw NotImplementedException();
}

bool User::joinRoom(Room* newRoom)
{
	if ( ! _currRoom) // if: NOT in a room
	{
		if (newRoom->joinRoom(this)) // if: joined the room succesfully
		{
			return true;
		}
	}

	return false;
}

void User::leaveRoom()
{
	if (_currRoom) // if: in a room
	{
		_currRoom->leaveRoom(this); // leave the room
		_currRoom = nullptr;
	}
}

int User::closeRoom()
{
	throw NotImplementedException();
}

bool User::leaveGame()
{
	throw NotImplementedException();
}
