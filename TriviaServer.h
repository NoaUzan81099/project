#pragma once
#include "User.h"
#include "RecievedMessage.h"
#include "Helper.h"
#include "Room.h"


class TriviaServer
{

private:

	SOCKET _socket;
	map<SOCKET, User*> _connectedUsers;

	////// NOT FOR NOW:
	// DataBase _db;
	
	map<int, Room*> _roomsList;
	mutex _mtxRecievedMessages;
	unique_lock<mutex> ulock;
	condition_variable _handleRecievedMessage;

	deque<RecievedMessage*> _queRcvMessages;


	static int _roomIdSequence;

public:
	TriviaServer();
	~TriviaServer();

	void serve();

private:

	void bindAndListen();
	void accept();
	void clientHandler(SOCKET);
	void safeDeleteUser(RecievedMessage*);

	User* handleSignin(RecievedMessage*);
	bool handleSignup(RecievedMessage*);
	void handleSignout(RecievedMessage*);

	void handleLeaveGame(RecievedMessage*);
	void handleStartGame(RecievedMessage*);
	void handlePlayerAnswer(RecievedMessage*);

	bool handleCreateRoom(RecievedMessage*);
	bool handleCloseRoom(RecievedMessage*);
	bool handleJoinRoom(RecievedMessage*);
	bool handleLeaveRoom(RecievedMessage*);
	void handleGetUsersInRoom(RecievedMessage*);
	void handleGetRooms(RecievedMessage*);

	void handleGetBestScores(RecievedMessage*);
	void handleGetPersonalStatus(RecievedMessage*);

	void handleRecievedMessages();
	void addRecievedMessage(RecievedMessage*);
	RecievedMessage* buildRecieveMessage(SOCKET, int);

	User* getUserByName(string);
	User* getUserBySocket(SOCKET);
	Room* getRoomById(int);

};
