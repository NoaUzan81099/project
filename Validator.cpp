#include "Validator.h"

bool Validator::isPasswordValid(string pass)
{
	int len = pass.length();
	bool check1 = false, check2 = false, check3 = false, check4 = true;
	if (len<4)
	{
		for (int i = 0; i < len; i++)
		{
			if (pass[i] >= 'a' && pass[i] <= 'z')
			{
				check1 = true;
			}
			if (pass[i] >= 'A' && pass[i] <= 'Z')
			{
				check2 = true;
			}
			if (pass[i] >= '0' && pass[i] <= '9')
			{
				check3 = true;
			}
			if (pass[i] == ' ')
			{
				check4 = false;
			}
		}
	}
	if (check1 && check2 && check3 && check4)
	{
		return true;
	}
	return false;
}

bool Validator::isUsernameValid(string name)
{
	bool check1 = false, check2 = true;
	if (name != "\0")
	{
		for (int i = 0; i < (int)name.length(); i++)
		{
			if (name[i] == ' ')
			{
				check2 = false;
			}
		}
		if (name[0] >= 'a' && name[0] <= 'z')
		{
			check1 = true;
		}
		if (check1 && check2)
			return true;
	}
	return false;
}