#pragma once

#include "Helper.h"
#include "Question.h"
#include "User.h"
#include "DataBase.h"

class User;

class Game
{

private:

	vector<Question*> _questions;
	vector<User*> _players;
	int _questions_no;
	int _currQuestionIndex;
	map<string, int> _results;
	int _currentTurnAnswers;

	/// NOT NOW:
	/// DataBase& _db;

public:

	//// REPLACE WITH DIFFERENT METHOD (NOT DB)
	//// Game(const vector<User*>&, int, DataBase&);

	~Game();

	void sendFirstQuestion();
	void handleFinishGame();
	bool handleNextTurn();
	bool handleAnswerFromUser(User*, int, int);
	bool leaveGame(User*);
	int getID();

private:

	bool insertGameToDB();
	void initQuestionsFromDB();
	void sendQuestionsToAllUsers();

};