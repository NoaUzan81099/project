#pragma once

#include "Helper.h"
#include "User.h"

using namespace std;


class RecievedMessage
{

private:

	SOCKET _sock;
	User* _user;
	int _messageCode;
	vector<string> _values;

public:

	RecievedMessage(SOCKET, int);
	RecievedMessage(SOCKET, int, vector<string>);

	SOCKET getSock();
	User* getUser();
	void setUser(User*);
	int getMessageCode();
	vector<string>& getValues();


};
