#pragma once

#include "Helper.h"
#include "Question.h"

class DataBase
{

private:

	static int callbackCount(void*, int, char**, char**);
	static int callbackQuestions(void*, int, char**, char**);
	static int callbackBestScores(void*, int, char**, char**);
	static int callbackPersonalStatus(void*, int, char**, char**);

public:
	
	DataBase();
	~DataBase();

	bool isUserExists(string);
	bool addNewUser(string, string, string);
	bool isUserAndPassMatch(string, string);
	
	vector<Question*> initQuestions(int);
	vector<string> getBestScores();

	int insertNewGame();
	bool updateGameStatus(int);
	
	bool addAnswerToPlayer(int, string, int, string, bool, int);

};