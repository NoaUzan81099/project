#pragma once
#include <iostream>
#include <vector>
#include "User.h"
using namespace std;

class User;

class Room
{
public:
	Room(int id, User* Admin, string name, int maxUsers, int questionNo, int questionTime);
	~Room();
	//should be changed by the protocol
	string getUsersListMessage();
	void leaveRoom(User* user);
	int closeRoom(User* user);
	bool joinRoom(User*);
	vector<User*> getUsers();
	int getQuestionsNo();
	int getId();
	string getName();


private:
	int _maxUsers;
	int _id;
	string _name;
	int _qustionNo;
	int _questionTime;
	std::vector < User* > vec;
	User* _admin;



};
