#pragma once


class Protocol
{
public:

	static const unsigned int PORT = 8820;
	static const unsigned int IFACE = 0;

	/********************
	 * CLIENT -> SERVER: 2xx
	 * SERVER -> CLIENT: 1xx
	*************/

	// sign options
	static const unsigned int RQST_SIGNIN = 200;
	static const unsigned int RQST_SIGNOUT = 201;
	static const unsigned int ANSWER_SIGNIN = 102;

	static const unsigned int RQST_SIGNUP = 203;
	static const unsigned int ANSWER_SIGNUP = 104;

	// rooms list
	static const unsigned int RQST_ROOMS_LIST = 205;
	static const unsigned int ANSWER_ROOMS_LIST = 106;

	// users in room list
	static const unsigned int RQST_USERS_IN_ROOM = 207;
	static const unsigned int ANSWER_USERS_IN_ROOM = 108;

	// join \ leave room
	static const unsigned int RQST_JOIN_ROOM = 209;
	static const unsigned int ANSWER_JOIN_ROOM = 110;

	static const unsigned int RQST_LEAVE_ROOM = 211;
	static const unsigned int ANSWER_LEAVE_ROOM = 112;

	// create \ close room
	static const unsigned int RQST_CREATE_ROOM = 213;
	static const unsigned int ANSWER_CREATE_ROOM = 114;

	static const unsigned int RQST_CLOSE_ROOM = 215;
	static const unsigned int ANSWER_CLOSE_ROOM = 116;

	// GAME START
	static const unsigned int RQST_START_GAME = 217;

	// questions
	static const unsigned int SEND_QUESTION = 118;
	static const unsigned int RCV_ANSWER = 219;
	static const unsigned int SEND_QUESTION_FEEDBACK = 120;

	// GAME END
	static const unsigned int SEND_GAME_ENDED = 121;

	static const unsigned int RQST_LEAVE_GAME = 222;
	
	// scores (my\all)
	static const unsigned int RQST_BEST_SCORES = 223;
	static const unsigned int ANSWER_BEST_SCORES = 124;

	static const unsigned int RQST_MY_SCORES = 225;
	static const unsigned int ANSWER_MY_SCORES = 126;

	// EXIT APPLICATION
	static const unsigned int RQST_EXIT = 299;

};